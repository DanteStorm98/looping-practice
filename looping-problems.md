# Problem 1: Print the numbers from 1 to 10
This problem is aimed at testing your understanding of loops in Python. You're required to print out all numbers from 1 to 10, inclusive. Here, we will utilize a for loop and the range function.

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize a loop that starts from 1 up to 10. On each iteration, print the current number.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
for i in range(1, 11):
    print(i)
```
</details>

# Problem 2: Print the even numbers between 1 and 20
This is an extension of the previous problem, but now you're required to print only the even numbers from 1 to 20.

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize a loop that starts from 2 up to 20 with a step of 2 (this will skip odd numbers). On each iteration, print the current number.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
for i in range(2, 21, 2):
    print(i)

# or

for i in range(1, 21):
    if i % 2 == 0:
        print(i)
```
</details>

# Problem 3: Print a right-angled triangle of height n using asterisks
In this problem, you'll use loops and string operations to print a right-angled triangle pattern.

<details>
  <summary>Reveal Pseudo Code</summary>
Loop from 1 to the triangle's height. On each iteration, print the asterisk symbol multiplied by the current number.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
n = 4
for i in range(1, n + 1):
    print("*" * i)
```
</details>

# Problem 4: Print a pyramid pattern of height n using asterisks
This problem is an extension of problem 3, but requires a pyramid pattern instead of a right-angled triangle.

<details>
  <summary>Reveal Pseudo Code</summary>
Loop from 1 to the pyramid's height. On each iteration, print spaces and asterisks such that the number of spaces decrease and the number of asterisks increase to form the pyramid shape.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
n = 4
for i in range(1, n + 1):
    spaces = " " * (n - i)
    stars = "*" * (2 * i - 1)
    print(f"{spaces}{stars}")
```
</details>

# Problem 5: Print a multiplication table for a given number n
This problem requires you to generate a multiplication table for a given number.

<details>
  <summary>Reveal Pseudo Code</summary>
Loop from 1 to 10, and on each iteration, print the product of the given number and the current number.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
n = 3
for i in range(1, 11):
    print(f"{n} * {i} = {n * i}")
```
</details>

# Problem 6: Find the sum of all numbers between 1 and n that are divisible by 3 or 5
This problem aims at finding the sum of numbers in a range that meet a specific condition.

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize a variable to 0 for storing the sum. Then loop from 1 to n, adding to the sum variable any number that is divisible by either 3 or 5.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
n = 10
total = 0
for i in range(1, n + 1):
    if i % 3 == 0 or i % 5 == 0:
        total += i
print(total)
```
</details>

# Problem 7: Check if a given number n is prime
This problem requires you to implement a basic primality test. Learn about [Prime numbers](https://en.wikipedia.org/wiki/Prime_number).

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize a boolean variable to True. Then loop from 2 to the square root of the number. If the number is divisible by any of these, set the boolean to False and break the loop.
</details>
<details>
  <summary>Reveal Solution</summary>

```py
n = 17
is_prime = True
for i in range(2, int(n ** 0.5) + 1):
    if n % i == 0:
        is_prime = False
        break
print(is_prime)
```
</details>

# Problem 8: Find the greatest common divisor (GCD) of two numbers a and b
This problem tests your ability to implement mathematical algorithms in Python. Learn about [Greatest Common Divisor](https://en.wikipedia.org/wiki/Greatest_common_divisor).

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize a variable to 1 to hold the GCD. Then loop from 1 to the minimum of the two numbers. On each iteration, if the current number divides both numbers, update the GCD to the current number.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
a = 56
b = 98
gcd = 1
for i in range(1, min(a, b) + 1):
    if a % i == 0 and b % i == 0:
        gcd = i
print(gcd)
```
</details>

# Problem 9: Print the first n Fibonacci numbers
This problem involves the generation of the first n numbers in the [Fibonacci sequence](https://en.wikipedia.org/wiki/Fibonacci_sequence).

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize two variables to the first two Fibonacci numbers (0 and 1). Then, in a loop of size n, print the first number, and update the two numbers to the second and the sum of the two numbers.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
n = 5
a, b = 0, 1
for _ in range(n):
    print(a)
    a, b = b, a + b
```
</details>

# Problem 10: Calculate the factorial of a given number n
This problem tests your understanding of mathematical computations in Python. The task is to compute the [factorial](https://en.wikipedia.org/wiki/Factorial) of a given number.

<details>
  <summary>Reveal Pseudo Code</summary>
Initialize a variable to 1 which will hold the factorial. Then, initiate a loop from 1 to the number. On each iteration, multiply the current number with the factorial variable.
</details>

<details>
  <summary>Reveal Solution</summary>

```py
n = 5
factorial = 1
for i in range(1, n + 1):
    factorial *= i
print(factorial)
```
</details>
